module ClientsHelper
  def client_name(client)
    "#{client.first_name} #{client.last_name}"
  end
end
