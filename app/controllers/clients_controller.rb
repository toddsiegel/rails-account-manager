class ClientsController < ApplicationController
  def show
    @client = Client.find(params[:id])
  end

  def new
    @client = Client.new
  end

  def create
   @client = Client.new(client_attributes)

   if @client.save
     redirect_to @client
   else
     render 'new'
   end
  end
 
  def index
    @clients = Client.all
  end

  def edit
    @client = Client.find(params[:id])
  end

  def update
    @client = Client.find(params[:id])

    if @client.update_attributes(client_attributes)
      redirect_to @client, notice: 'This client was updated.'
    else
      render 'edit'
    end
  end

  def destroy
    client = Client.find(params[:id])
    client.destroy
    
    redirect_to clients_path
  end

  private
 
  def client_attributes
    params.require(:client).permit(:first_name, :last_name, :email)
  end
end
