class Account < ApplicationRecord
  belongs_to :client

  def client_name
    "#{client.first_name} #{client.last_name}"
  end
end
